package com.bostongene.test.user_service.users;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class InMemoryUserDAOTest {

    UserDAO dao = InMemoryUserDAO.getInstance();

    @After
    public void resetMemory () {
        for (User u : dao.getUsers())
            dao.removeUser(u);
    }

    @Test
    public void addUserTest () {
        User testUser = new User(
                "Иван",
                "Иванов",
                "01.01.1970",
                "mail@example.com",
                "qwerty123"
        );

        dao.addUser(testUser);
        assertNotNull(dao.getUser("mail@example.com"));
        assertNull(dao.getUser("spam@example.com"));
    }

    @Test (expected=RuntimeException.class)
    public void noDuplicateTest () {
        User testUser = new User(
                "Вася",
                "Городничев",
                "01.01.1970",
                "vasya@example.com",
                "88005553535"
        );

        dao.addUser(testUser);
        dao.addUser(testUser);
    }

    @Test
    public void removeUserTest () {
        User testUser = new User(
                "Пётр",
                "Помидоров",
                "01.01.1970",
                "peter@example.com",
                "54321"
        );

        dao.addUser(testUser);
        assertNotNull(dao.getUser("peter@example.com"));
        dao.removeUser(testUser);
        assertNull(dao.getUser("peter@example.com"));
    }

    @Test
    public void getUserTest () {
        User testUser = new User(
                "Александр",
                "Македонский",
                "01.01.1970",
                "alex1337@example.com",
                "AlexBrutal777"
        );
        User testUser2 = new User(
                "Наполеон",
                "Бонопарт",
                "01.01.1970",
                "polya@example.com",
                "aEZakMI"
        );

        assertNull(dao.getUser("alex1337@example.com"));
        assertNull(dao.getUser("polya@example.com"));
        dao.addUser(testUser);
        assertNotNull(dao.getUser("alex1337@example.com"));
        assertNull(dao.getUser("polya@example.com"));
        dao.addUser(testUser2);
        assertNotNull(dao.getUser("alex1337@example.com"));
        assertNotNull(dao.getUser("polya@example.com"));

        User testUser3 = dao.getUser("polya@example.com");
        assertEquals(testUser2, testUser3);
        assertNotEquals(testUser, testUser3);
    }
}
