package com.bostongene.test.user_service.users;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void userCreateTest () {
        User user = new User(
                "Викентий",
                "Виноградов",
                "01.01.1970",
                "vic@example.com",
                "P@sSw0rD"
        );

        assertNotNull(user);
        assertTrue(user.checkPassword("P@sSw0rD"));
        assertEquals(user.getEmail(), "vic@example.com");
        assertEquals(user.getFirstName(), "Викентий");
        assertEquals(user.getLastName(), "Виноградов");
    }

    @Test
    public void passwordChangeTest () {
        User user = new User(
                "Викентий",
                "Виноградов",
                "01.01.1970",
                "vic@example.com",
                "P@sSw0rD"
        );

        assertTrue(user.checkPassword("P@sSw0rD"));

        user.setPassword("123456");

        assertFalse(user.checkPassword("P@sSw0rD"));
        assertTrue(user.checkPassword("123456"));
    }

    @Test
    public void notEqualsTest () {
        User user = new User(
                "Виталий",
                "Кошкин",
                "01.01.1970",
                "mew@example.com",
                "7899987"
        );
        User user2 = new User(
                "Александр Викторович",
                "Собакин",
                "01.01.1970",
                "gav@example.com",
                "98986438"
        );

        assertNotEquals(user, user2);
        assertNotNull(user);
        assertNotNull(user2);
    }
}
