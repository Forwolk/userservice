package com.bostongene.test.user_service.provider;

import com.bostongene.test.user_service.logging.LogManager;
import com.bostongene.test.user_service.users.InMemoryUserDAO;
import com.bostongene.test.user_service.users.User;
import com.bostongene.test.user_service.users.UserDAO;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("users")
public class ApiProvider {

    LogManager log = new LogManager(ApiProvider.class);

    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/plain")
    public Response register(@FormParam("firstname") String firstname,
                             @FormParam("lastname") String lastname,
                             @FormParam("email") String email,
                             @FormParam("birthday") String birthday,
                             @FormParam("password") String password,
                             @Context HttpServletRequest request) {
        if (
                firstname == null || firstname.equals("") ||
                lastname == null || lastname.equals("") ||
                birthday == null || birthday.equals("") ||
                email == null || email.equals("") ||
                password == null || password.equals("")) {
            log.info(String.format("Bad request from %s.", request.getRemoteHost()));
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        User user = new User (
                firstname,
                lastname,
                birthday,
                email,
                password
        );

        try {
            UserDAO dao = InMemoryUserDAO.getInstance();
            dao.addUser(user);
        } catch (RuntimeException e) {
            log.info(String.format("User %s is already registered. Request from %s.", user, request.getRemoteHost()));
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        log.info(String.format("User %s is successfully registered by request from %s.", user, request.getRemoteHost()));
        return Response.ok("User created.").build();
    }

    @POST
    @Path("get")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response getUser (@FormParam("email") String email,
                             @Context HttpServletRequest request) {
        User user = InMemoryUserDAO.getInstance().getUser(email);
        if (user == null) {
            log.info(String.format("User with e-mail %s not found. Request from %s.", email, request.getRemoteHost()));
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }
        log.info(String.format("Get information about %s by request from %s.", user, request.getRemoteHost()));
        return Response.ok(user.toString()).build();
    }

    @POST
    @Path("remove")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/plain")
    public Response removeUser (@FormParam("email") String email,
                             @Context HttpServletRequest request) {
        User user = InMemoryUserDAO.getInstance().getUser(email);
        if (user == null) {
            log.info(String.format("User with e-mail %s not found. Request from %s.", email, request.getRemoteHost()));
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        }

        InMemoryUserDAO.getInstance().removeUser(user);
        log.info(String.format("User %s is successfully removed by request from %s.", user, request.getRemoteHost()));
        return Response.ok("User removed.").build();
    }
}
