package com.bostongene.test.user_service.security;

/**
 * Класс, предоставляющий возможности защиты данных
 */
public class Security {

    /**
     * MD-5 hash generator
     *
     * @param string строка, от которой нужно взять хэш
     * @return хэш MD-5
     */
    public static String MD5 (String string) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(string.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
