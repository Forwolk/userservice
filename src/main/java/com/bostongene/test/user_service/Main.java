package com.bostongene.test.user_service;

import com.bostongene.test.user_service.api.ApiServlet;
import com.bostongene.test.user_service.logging.LogManager;

public class Main {

    static LogManager log = new LogManager(Main.class);

    public static void main (String... args) {
        try {
            ApiServlet.start();
        } catch (Exception e) {
            log.error(e);
        }
    }
}
