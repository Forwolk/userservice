package com.bostongene.test.user_service.api;

import com.bostongene.test.user_service.logging.LogManager;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ApiServlet {
    private static int port = 8888;

    private static LogManager log = new LogManager(ApiServlet.class);

    public static void start () throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        Server jettyServer = new Server(port);
        jettyServer.setHandler(context);

        ServletHolder jerseyServlet = context.addServlet(
                org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);

        jerseyServlet.setInitParameter(
                "jersey.config.server.provider.packages",
                "com.bostongene.test.user_service.provider"
        );

        try {
            jettyServer.start();
            String host = jettyServer.getURI().getHost();
            log.info(String.format("Server {ip: %s} is running on port %s.", host, port));
            jettyServer.join();
        } finally {
            jettyServer.destroy();
        }
    }
}
