package com.bostongene.test.user_service.users;

import com.bostongene.test.user_service.logging.LogManager;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class InMemoryUserDAO implements UserDAO {

    private LogManager log = new LogManager(InMemoryUserDAO.class);

    private static InMemoryUserDAO inMemoryUserDAO;

    /**
     * Синглтон как-никак
     *
     * @return объект InMemoryUserDAO
     */
    public static InMemoryUserDAO getInstance () {
        if (inMemoryUserDAO == null)
            inMemoryUserDAO = new InMemoryUserDAO();
        return inMemoryUserDAO;
    }

    private InMemoryUserDAO () {}

    /** Хранимые памятью пользователи */
    private HashMap <String, User> users = new HashMap<String, User>();

    /**
     * Добавление пользователя
     *
     * @param user добавляемый пользователь
     */
    public void addUser(User user) {
        log.info("Trying to add user " + user + "...");
        if (users.containsKey(user.getEmail().toLowerCase())) {
            log.error("User is already added in memory!");
            throw new RuntimeException("Пользователь уже есть в системе!");
        }
        users.put(user.getEmail().toLowerCase(), user);
        log.info("User successfully added.");
    }

    /**
     * Удаление пользователя
     *
     * @param user удаляемый пользователь
     */
    public void removeUser(User user) {
        users.remove(user.getEmail());
        log.info("User removed: " + user);
    }

    /**
     * Поиск пользователя по e-mail
     *
     * @param email e-mail
     * @return искомый пользователь
     */
    public User getUser(String email) {
        return users.get(email.toLowerCase());
    }

    /**
     * Получение всех пользователей
     *
     * @return сет пользователей
     */
    public Set<User> getUsers() {
        Set <User> userSet = new HashSet<User>();
        for (String mail : users.keySet())
            userSet.add(this.getUser(mail));
        return userSet;
    }
}
