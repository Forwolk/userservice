package com.bostongene.test.user_service.users;

import java.util.Set;

public interface UserDAO {

    /**
     * Добавление пользователя
     *
     * @param user добавляемый пользователь
     */
    void addUser (User user);

    /**
     * Удаление пользователя
     *
     * @param user удаляемый пользователь
     */
    void removeUser (User user);

    /**
     * Поиск пользователя по e-mail
     *
     * @param email e-mail
     * @return искомый пользователь
     */
    User getUser (String email);

    /**
     * Получение всех пользователей
     *
     * @return сет пользователей
     */
    Set<User> getUsers ();
}
