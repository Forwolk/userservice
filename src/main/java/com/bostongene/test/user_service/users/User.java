package com.bostongene.test.user_service.users;

import com.bostongene.test.user_service.security.Security;
import com.google.gson.GsonBuilder;

/**
 * Класс пользователя
 */
public class User {

    /** Имя */
    private final String firstName;
    /** Фамилия */
    private final String lastName;
    /** Дата рождения */
    private final String dayOfBirth;
    /** E-mail */
    private final String email;
    /** Пароль (хэш от пароля с именем пользователя) */
    private String password;

    public User (
            String firstName,
            String lastName,
            String dayOfBirth,
            String email,
            String password
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dayOfBirth = dayOfBirth;
        this.email = email;
        this.setPassword(password);
    }


    /**
     * Получение имени
     *
     * @return имя
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Получение фамилии
     *
     * @return фамилия
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Получение даты рождения
     *
     * @return дата рождения
     */
    public String  getDayOfBirth() {
        return dayOfBirth;
    }

    /**
     * Проверка совпадения пароля
     *
     * @param password пароль в незашифрованном виде
     * @return true - при совпадении, иначе - false
     */
    public boolean checkPassword(String password) {
        return getPasswordHash(password).equals(this.password);
    }

    /**
     * Установка пароля
     *
     * @param password пароль в незашифрованном виде
     */
    public void setPassword(String password) {
        this.password = getPasswordHash(password);
    }

    /**
     * Получение e-mail адреса пользователя
     *
     * @return e-mail
     */
    public String getEmail() {
        return email;
    }

    /**
     * Получение хэша от связки пароль + e-mail
     *
     * @param password незашифрованный пароль
     * @return хэш связки
     */
    private String getPasswordHash (String password) {
        password = password + email.toLowerCase();
        password = Security.MD5(password);
        return password;
    }

    @Override
    public String toString () {
        return new GsonBuilder().create().toJson(this);
    }
}
