package com.bostongene.test.user_service.logging;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogManager {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String className;
    public static Logger log = Logger.getLogger("Log manager");

    public LogManager (Class className) {
        this.className = className.getName();
    }

    public void log(Level logLevel, Object obj){
        String time = SDF.format(Calendar.getInstance().getTime());
        if (logLevel.equals(Level.SEVERE))
            System.err.println(String.format("[%s|%s] :: %s", logLevel.toString(), time, obj));
        else
            System.out.println(String.format("[%s|%s] :: %s", logLevel.toString(), time, obj));
        log.log(logLevel, String.format("%s :: %s", className, obj));
    }

    public void info (Object obj){
        this.log(Level.INFO, obj);
    }

    public void error (Object obj){
        if (obj instanceof Exception) {
            ((Exception) obj).printStackTrace();
            for (StackTraceElement s : ((Exception) obj).getStackTrace()){
                log.severe(s.toString());
            }
        }
        log(Level.SEVERE, obj);
    }

    static {
        try {
            java.util.logging.LogManager.getLogManager().readConfiguration(LogManager.class.getResourceAsStream("/logger.properties"));
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
